### Project Tracker

### Submitted By: Sunil Prajapati
### ID: 124073

A rails app for learning unit testing.

## Pre-requisites:

- Ruby
- Rails
- Postgresql

## Running the app

To run the app, first install all the gem packages:

`bundle install`

Create the database from the command:

`rake db:create`

Migrate the databse using:

`rake db:migrate`

To run the run, use the command:

`rails server`


## Testing the app

To run the test, use the command

`rake test`